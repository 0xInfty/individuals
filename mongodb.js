const  MongoClient = require("mongodb").MongoClient;

let db;

async function connectToServer() {
    const client = await MongoClient.connect( "mongodb://mongo:27017/individuals", { useUnifiedTopology: true, useNewUrlParser: true });
    db =  client.db("individuals");
    if (db) {
      return;
    } else {
      throw new Error ("Fatal error, couldn't connect to MongoDB");
    }
}

function getDb() {
  return db;
}

module.exports = {connectToServer, getDb}
