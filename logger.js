const winston = require('winston')

const logDomain = process.env.LOGS_HOSTNAME
const logsPort = process.env.LOGS_PORT
const serviceName = process.env.SERVICE_NAME

const production = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json(),
    ),
    defaultMeta: { service: serviceName},
    transports: [
        new winston.transports.File({ 
            filename: './logs/all.log', 
            level: 'verbose',
            maxsize: 1024*1024, //1 Mb
            maxFiles: 5,        //5 concurrent files
        }),
        new winston.transports.File({ 
            filename: './logs/errors.log', 
            level: 'warn',
            maxsize: 1024*1024, //1 Mb
            maxFiles: 5,        //5 concurrent files
        }),
        new winston.transports.Http({
            level: 'info' ,
            host: logDomain,
            port: logsPort,
            path: '/create',
        }),
        new winston.transports.Console({
            level: 'debug',
        }),
    ],
});

const development = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json(),
        winston.format.prettyPrint(),
        winston.format.colorize({ all: true }),
    ),
    defaultMeta: { service: serviceName},
    transports: [
        new winston.transports.Console({
            level: 'debug',
        }),
    ],

});

if(process.env.NODE_ENV==='production'){
    exports.logger =production
}
else{
    exports.logger =development
}
